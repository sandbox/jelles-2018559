INSTRUCTIONS
-------------
Download the module.
Download the Gridster javascript library (http://gridster.net).
Place it under sites/all/libraries/gridster so that the files can be found under sites/all/libraries/gridster/src/jquery.gridster.js and sites/all/libraries/gridster/src/jquery.gridster.min.js.
Enable the module.


This module does nothing by itself, it just provides the gridster library so that other modules can use it.

You can load the library like this:
<?php
libraries_load('gridster');
?>
or
<?php
libraries_load('gridster', 'minified');
?>
for the minified version (make sure you have jquery.gridster.min.js in sites/all/libraries/gridster/src).
